package com.atlassian.confluence.extra.calendar;

import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.renderer.links.Link;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.extra.calendar.model.ICalendar;
import com.atlassian.confluence.extra.calendar.display.CalendarDisplay;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.links.linktypes.AbstractPageLink;
import com.atlassian.user.User;

import java.util.regex.Pattern;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.Period;

public class CalendarEventsMacro extends BaseMacro {
    private static final Pattern ID_PATTERN = Pattern.compile( "\\w+" );

    private static final Logger log = Logger.getLogger( CalendarEventsMacro.class );

    private CalendarManager calendarManager;

    private BootstrapManager bootstrapManager;

    private PageManager pageManager;

    private LinkResolver linkResolver;

    public boolean isInline() {
        return false;
    }

    public boolean hasBody() {
        return false;
    }

    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    public String execute( Map map, String string, RenderContext renderContext ) throws MacroException {
        PageContext pageContext = ( PageContext ) renderContext;

        // Am I debugging?
        boolean debug = "true".equals( map.get( "debug" ) );

        // determine the page title and space key
        String pageString = ( String ) map.get( "page" );
        Link link = linkResolver.createLink( renderContext, pageString );

        if ( !( link instanceof AbstractPageLink ) ) {
            throw new MacroException( "Unable to locate the page using the parameter passed." );
        }

        AbstractPageLink pageLink = ( AbstractPageLink ) link;

        // find the content entity object
        ContentEntityObject content = pageManager.getPage( pageLink.getSpaceKey(), pageLink.getPageTitle() );
        if ( content == null ) {
            throw new MacroException( "Unable to find the page " + pageLink.getSpaceKey() + ":"
                    + pageLink.getPageTitle() );
        }

        // now find the _group
        String id = ( String ) map.get( "calendarId" );
        if ( id == null )
            throw new MacroException( "Please supply the id of the calendar" );
        if ( !ID_PATTERN.matcher( id ).matches() )
            throw new MacroException( "The calendar id can only contain letters or numbers" );

        // Grab the calendar
        ICalendar calendar = getCalendarManager().getCalendar( content, id );
        if ( calendar == null ) {
            throw new MacroException( "Unable to find a calendar by that ID on page described." );
        }

        // Get the timezone
        User user = AuthenticatedUserThreadLocal.getUser();

        TimeZone timeZone = CalendarManager.getInstance().getTimeZone( user );
        DateTimeZone dateTimeZone = CalendarManager.getInstance().getDateTimeZone( timeZone );

        // Get the local date today
        LocalDate today = new LocalDate( dateTimeZone );

        // setup our model for display of the event calendar
        CalendarDisplay calendarDisplay = new CalendarDisplay( calendar, today, 0, timeZone, 20, Period.years( 1 ) );

        // Set the context path
        String contextPath = renderContext.getSiteRoot();
        if ( contextPath == null )
            contextPath = bootstrapManager.getWebAppContextPath();

        // Get resource bundle
        ResourceBundle resources = CalendarManager.getInstance().getResourceBundle( user );

        // now create a simple velocity context and render a template for the
        // output
        Map contextMap = MacroUtils.defaultVelocityContext();
        contextMap.put( "calendar", calendar );
        contextMap.put( "content", content );
        contextMap.put( "calendarManager", getCalendarManager() );
        contextMap.put( "calendarDisplay", calendarDisplay );
        contextMap.put( "contextPath", contextPath );
        contextMap.put( "debug", ( debug ) ? Boolean.TRUE : Boolean.FALSE );
        contextMap.put( "spaceKey", pageContext.getSpaceKey() );
        contextMap.put( "resources", resources );
        contextMap.put( "remoteUser", user );

        try {
            return VelocityUtils.getRenderedTemplate( "templates/extra/calendar/calendar-events.vm", contextMap );
        } catch ( Exception e ) {
            log.error( "Error while trying to display calendar!", e );
            throw new MacroException( e.getMessage() );
        }
    }

    protected CalendarManager getCalendarManager() {
        if ( calendarManager == null )
            calendarManager = CalendarManager.getInstance();

        return calendarManager;
    }

    public void setBootstrapManager( BootstrapManager bootstrapManager ) {
        this.bootstrapManager = bootstrapManager;
    }

    public void setPageManager( PageManager pageManager ) {
        this.pageManager = pageManager;
    }

    public void setLinkResolver( LinkResolver linkResolver ) {
        this.linkResolver = linkResolver;
    }

}
