/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar.action;

import com.atlassian.confluence.extra.calendar.CalendarHandler;
import com.atlassian.confluence.extra.calendar.model.CalendarException;
import com.atlassian.confluence.extra.calendar.model.CalendarGroup;
import com.atlassian.confluence.extra.calendar.model.ICalendar;

/**
 * Provides methods and fields useful to actions dealing with sub-calendars.
 */
public abstract class AbstractSubCalendarAction extends AbstractCalendarAction {
    protected String subCalendarId;

    private ICalendar _subCalendar;

    private String targetNamespace;

    protected AbstractSubCalendarAction( boolean requireEditPermission ) {
        super( requireEditPermission );
    }

    public String execute() throws Exception {
        return INPUT;
    }

    public String getSubCalendarId() {
        return subCalendarId;
    }

    public void setSubCalendarId( String subCalendarId ) {
        this.subCalendarId = subCalendarId;
    }

    protected void setSubCalendar( ICalendar subCalendar ) {
        this._subCalendar = subCalendar;
    }

    public ICalendar getSubCalendar() {
        if ( _subCalendar == null && subCalendarId != null ) {
            CalendarGroup group = getCalendar();
            if ( group != null ) {
                _subCalendar = group.getDescendent( subCalendarId );

                if ( _subCalendar != null ) {
                    try {
                        _subCalendar.init();
                    } catch ( CalendarException e ) {
                        e.printStackTrace();
                    }
                }

            }
        }
        return _subCalendar;
    }

    /**
     * Handles performs the specified action and returns the status.
     * 
     * @param action
     *            The action to perform.
     * @return The status (SUCCESS/ERROR/etc).
     */
    protected String handleAction( int action ) {
        String calendarClassname = getSubCalendarClassname();

        if ( targetNamespace == null && calendarClassname != null ) {
            CalendarHandler handler = getCalendarManager().findHandler( calendarClassname );

            targetNamespace = handler.getNamespace();
            return SUCCESS;
        }
        addActionError( "Unable to locate the specified calendar." );
        return ERROR;
    }

    protected String getSubCalendarClassname() {
        ICalendar subCalendar = getSubCalendar();
        if ( subCalendar != null )
            return subCalendar.getClass().getName();
        return null;
    }

    public void setTargetNamespace( String targetNamespace ) {
        this.targetNamespace = targetNamespace;
    }

    public String getTargetNamespace() {
        return targetNamespace;
    }
}
