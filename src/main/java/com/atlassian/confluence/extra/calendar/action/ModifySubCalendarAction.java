/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar.action;

import com.atlassian.confluence.extra.calendar.CalendarHandler;
import com.atlassian.confluence.extra.calendar.model.CalendarGroup;
import com.atlassian.confluence.extra.calendar.model.ICalendar;

import java.util.List;

/**
 * Created by IntelliJ IDEA. User: david Date: Nov 29, 2005 Time: 4:04:25 PM To
 * change this template use File | Settings | File Templates.
 */
public class ModifySubCalendarAction extends AbstractSubCalendarAction {
    private String calendarClassname;

    public ModifySubCalendarAction() {
        super( true );
    }

    public String addCalendar() {
        if ( !isEditPermitted() )
            return PAGE_NOT_PERMITTED;

        if ( calendarClassname == null ) {
            List handlers = getCalendarManager().getHandlers();
            switch ( handlers.size() ) {
                case 0:
                    addActionError( "Unable to find any calendar types to add." );
                    return ERROR;
                case 1: // there is only one handler - skip the selection page.
                    calendarClassname = ( ( CalendarHandler ) handlers.get( 0 ) ).getCalendarClass().getName();
                    return handleAction( CalendarHandler.ADD_CALENDAR );
                default:
                    return INPUT;
            }
        }

        return handleAction( CalendarHandler.ADD_CALENDAR );
    }

    public String editCalendar() {
        if ( !isEditPermitted() )
            return PAGE_NOT_PERMITTED;

        return handleAction( CalendarHandler.EDIT_CALENDAR );
    }

    public String deleteCalendar() {
        if ( !isEditPermitted() )
            return PAGE_NOT_PERMITTED;

        ICalendar cal = getSubCalendar();
        CalendarGroup group = getCalendar();

        if ( group != null && cal != null ) {
            if ( group.removeDescendent( cal ) ) {
                return saveCalendar();
            }
        }

        addActionError( "Unable to delete the calendar." );
        return ERROR;
    }

    public String getCalendarClassname() {
        return calendarClassname;
    }

    public void setCalendarClassname( String calendarClassname ) {
        this.calendarClassname = calendarClassname;
    }

    protected String getSubCalendarClassname() {
        String scc = super.getSubCalendarClassname();
        if ( scc == null )
            return calendarClassname;
        return scc;
    }
}
