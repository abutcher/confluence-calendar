package com.atlassian.confluence.extra.calendar.extractor;

public interface ICalendarExtractor {

    public final static String EVENT_SUMMARY = "event_summary";

    public final static String EVENT_LOCATION = "event_location";

    public final static String EVENT_DESC = "event_desc";

    public final static String EVENT_LINK = "event_link";

    public final static String EVENT_START_DATE = "event_start_date";

    public final static String EVENT_END_DATE = "event_end_date";

    public final static String CALENDAR_NAME = "calendar_name";

    public final static String CALENDAR_DESC = "calendar_desc";

    public final static String CALENDAR_COLOUR = "calendar_color";

    public final static String CALENDAR_TYPE = "calendar_type";

    public final static String CALENDAR_LOCATION = "calendar_location";

    public static final String CALENDAR_MARCO = "{calendar";

    public static final String CALENDAR_ID = "id";

    public static final String CALENDAR_SEPARATOR = "|";

    public static final String CALENDAR_EQUALS = "=";

    public static final String SPACE = " ";

}
