/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.atlassian.confluence.extra.calendar.model;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;

/**
 * An abstract base class of {@link IEvent} providing implementations for common
 * methods.
 * 
 * @author David Peterson
 */
public abstract class AEvent implements IEvent {
    /**
     * Returns the start date/time of this event, aligned with the specified
     * timezone.
     * 
     * @param targetTimeZone
     *            The timezone the date/time should be aligned with.
     * @return The exact start date/time in the specified timezone.
     * @throws NullPointerException
     *             if the targetTimeZone is <code>null</code>.
     */
    public DateTime getStartDateTime( DateTimeZone targetTimeZone ) {
        return getDateTime( getStartDate(), targetTimeZone );
    }

    /**
     * Returns the end date/time of this event, aligned with the specified
     * timezone.
     * 
     * @param targetTimeZone
     *            The timezone the date/time should be aligned with.
     * @return The exact start date/time in the specified timezone.
     * @throws NullPointerException
     *             if the targetTimeZone is <code>null</code>.
     */
    public DateTime getEndDateTime( DateTimeZone targetTimeZone ) {
        return getDateTime( getEndDate(), targetTimeZone );
    }

    /**
     * Converts the specified local time to the target timezone, via the event's
     * own timezone.
     * 
     * @param local
     *            The local time to convert.
     * @param targetTimeZone
     *            The target timezone.
     * @return The exact date/time of the local date/time, applied to the target
     *         timezone.
     */
    protected DateTime getDateTime( LocalDateTime local, DateTimeZone targetTimeZone ) {
        if ( targetTimeZone == null )
            throw new NullPointerException( "targetTimeZone may not be null" );

        if ( isAllDay() ) // All day event
        {
            return local.toLocalDate().toDateMidnight( targetTimeZone ).toDateTime();
        } else {
            DateTimeZone evtTimeZone = getTimeZone();
            if ( evtTimeZone != null ) // it's a specific timezone.
                return local.toDateTime( evtTimeZone ).toDateTime( targetTimeZone );
            else
                // it's a floating timezone. Use the target directly.
                return local.toDateTime( targetTimeZone );
        }
    }

}
