/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar.model;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

/**
 * This is a concrete implementation of {@link IParentCalendar} which simply
 * agregates a number of other calendars together.
 */
public class CalendarGroup extends AParentCalendar {
    private List children;

    /**
     * A list of the children that should not be displayed
     */
    private List inactive;

    public CalendarGroup() {
        children = new LinkedList();
        inactive = new LinkedList();
    }

    public void addChild( ICalendar calendar ) {
        children.add( calendar );
    }

    public boolean removeChild( ICalendar calendar ) {
        return children.remove( calendar );
    }

    public List getChildren() {
        return Collections.unmodifiableList( children );
    }

    public List getEditableChildren() {
        List ec = new java.util.LinkedList();
        Iterator i = children.iterator();
        ICalendar cal;
        while ( i.hasNext() ) {
            cal = ( ICalendar ) i.next();
            if ( inactive.contains( cal.getId() ) == false ) {
                if ( !cal.isReadOnly() )
                    ec.add( cal );
            }
        }
        return ec;
    }

    public void init() throws CalendarException {
        if ( inactive == null ) {
            // This is required to initialise calendars
            // that were saved in versions before 2.4
            inactive = new LinkedList();
        }
    }

    /**
     * This method initialises all the child Calendars
     */
    public void initChildren() {
        Iterator i = children.iterator();
        ICalendar cal;

        while ( i.hasNext() ) {
            cal = ( ICalendar ) i.next();
            try {
                cal.init();
            } catch ( CalendarException e ) {
                e.printStackTrace();
            }

            if ( cal instanceof CalendarGroup ) {
                ( ( CalendarGroup ) cal ).initChildren();
            }

        }
    }

    public void commit() throws CalendarException {
        Iterator i = children.iterator();
        ICalendar cal;

        while ( i.hasNext() ) {
            cal = ( ICalendar ) i.next();
            cal.commit();
        }
    }

    /**
     * Allows the ID of this calendar to be set manually. It is the setter's
     * responsibility to ensure it is a unique ID.
     * 
     * @param id
     *            The ID.
     */
    public void setId( String id ) {
        super.setId( id );
    }

    /**
     * This method does its best to find the event based on the UID.
     * 
     * @param id
     *            The ID of the event to find.
     * @return The event, or <code>null</code> if it could not be found.
     */
    public IEvent findEvent( String id ) {
        Iterator i = children.iterator();
        IEvent event = null;
        ICalendar cal;

        while ( i.hasNext() && event == null ) {
            try {
                cal = ( ICalendar ) i.next();
                if ( inactive.contains( cal.getId() ) == false ) {
                    event = cal.findEvent( id );
                }
            } catch ( CalendarException e ) {
                e.printStackTrace();
            }
        }

        return event;
    }

    /**
     * This method does its best to find the base event based on the UID.
     * 
     * @param id
     *            The ID of the event to find.
     * @return The event, or <code>null</code> if it could not be found.
     */
    public IEvent findBaseEvent( String id ) {
        Iterator i = children.iterator();
        IEvent event = null;
        ICalendar cal;

        while ( i.hasNext() && event == null ) {
            try {
                cal = ( ICalendar ) i.next();
                if ( inactive.contains( cal.getId() ) == false ) {
                    event = cal.findBaseEvent( id );
                }
            } catch ( CalendarException e ) {
                e.printStackTrace();
            }
        }

        return event;
    }
    /**
     * Returns an agregation of the events for all children in the specified
     * interval.
     * 
     * @param interval
     *            The interval of time to retrieve events for.
     * @return The events.
     */
    public List findEvents( Interval interval, DateTimeZone targetTimeZone ) {
        List events = new LinkedList();
        Iterator i = children.iterator();
        ICalendar cal;

        while ( i.hasNext() ) {
            cal = ( ICalendar ) i.next();
            try {
                if ( inactive.contains( cal.getId() ) == false ) {
                    events.addAll( cal.findEvents( interval, targetTimeZone ) );
                }
            } catch ( CalendarException e ) {
                e.printStackTrace();
            }
        }

        return events;
    }

    public boolean isReadOnly() {
        return true;
    }

    public boolean removeDescendent( ICalendar cal ) {
        Iterator i = children.iterator();
        ICalendar child;

        while ( i.hasNext() ) {
            child = ( ICalendar ) i.next();
            if ( child == cal ) {
                return removeChild( child );
            } else if ( child instanceof IParentCalendar ) {
                if ( ( ( IParentCalendar ) child ).removeDescendent( cal ) )
                    return false;
            }
        }
        return false;
    }

    public boolean isUndisplayable( ICalendar cal ) {
        if ( inactive.contains( cal.getId() ) == true ) {
            return true;
        } else {
            return false;
        }
    }

    public void clearUndisplayable( ICalendar cal ) {
        if ( inactive.contains( cal.getId() ) == true ) {
            inactive.remove( cal.getId() );
        }
    }

    public void setUndisplayable( ICalendar cal ) {
        inactive.add( cal.getId() );
    }

}
