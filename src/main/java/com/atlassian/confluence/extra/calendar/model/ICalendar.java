/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.calendar.model;

import org.joda.time.Interval;
import org.joda.time.DateTimeZone;

import java.util.List;

/**
 * The teneral interface for a calendar.
 */
public interface ICalendar {
    /**
     * Initialises the calendar.
     * 
     * @throws CalendarException
     *             if there is a problem initialising.
     */
    void init() throws CalendarException;

    /**
     * Returns the unique identifier for this calendar.
     * 
     * @return The unique ID.
     */
    String getId();

    /**
     * Returns the name of the calendar.
     * 
     * @return The calendar name.
     */
    String getName();

    /**
     * Sets the name of the calendar.
     * 
     * @param name
     *            The new name.
     */
    void setName( String name );

    /**
     * The description of the calendar.
     * 
     * @return the calendar description.
     */
    String getDescription();

    /**
     * Change the calendar description.
     * 
     * @param description
     *            The new calendar description.
     */
    void setDescription( String description );

    /**
     * Finds the event in this calendar with the specified id.
     * 
     * @param id
     *            The ID of the event to find.
     * @return the event, or <code>null</code> if none could be found.
     * @throws CalendarException
     *             if there was a problem while searching.
     */
    IEvent findEvent( String id ) throws CalendarException;

    /**
     * Finds the base event in this calendar with the specified id.
     * 
     * @param id
     *            The ID of the event to find.
     * @return the event, or <code>null</code> if none could be found.
     * @throws CalendarException
     *             if there was a problem while searching.
     */
    IEvent findBaseEvent( String id ) throws CalendarException;

    /**
     * Finds the list of events which occur in the specified time interval.
     * 
     * @param interval
     *            The interval of time to look in.
     * @param targetTimeZone
     *            The time zone the interval applies to.
     * @return the list of events.
     * @throws CalendarException
     *             if there was a problem searching.
     */
    List findEvents( Interval interval, DateTimeZone targetTimeZone ) throws CalendarException;

    /**
     * If <code>true</code>, the calendar cannot be commited.
     * 
     * @return <code>true</code> if the calendar can be commited.
     */
    boolean isReadOnly();

    /**
     * Commits the changes made in the calendar.
     * 
     * @throws CalendarException
     *             if there is a problem committing.
     */
    void commit() throws CalendarException;

    /**
     * Returns the CSS-friendly colour for the calandar's events.
     * 
     * @return The calendar's color.
     */
    String getColor();

    /**
     * Setting a value for the suppressSubscribLink will suppress the Subscribe link from showing in the calendar view.
     * A null value permites the Subscribe link to be shown.
     *
     * @param suppressSubscribeLink
     */
    void setSuppressSubscribeLink(String suppressSubscribeLink);

    String getSuppressSubscribeLink();
}
