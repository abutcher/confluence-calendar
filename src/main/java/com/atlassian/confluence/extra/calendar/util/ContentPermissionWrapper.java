package com.atlassian.confluence.extra.calendar.util;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ContentPermissionWrapper {

    private static final Logger logger = Logger.getLogger( ContentPermissionWrapper.class );

    private Object wrapped;

    public ContentPermissionWrapper( final Object contentPermission ) {
        setWrapped( contentPermission );
    }

    protected Object getWrapped() {
        return wrapped;
    }

    protected void setWrapped( Object wrapped ) {
        this.wrapped = wrapped;
    }

    protected Class getWrappedClass() {
        return getWrapped().getClass();
    }

    /*
     * If you need to wrap methods of the original ContentEntity (be it 2.3 or >
     * 2.3), make use of this
     */
    protected Object invokeWrappedMethod( final String methodName, final Class[] parameterTypes,
            final Object[] values ) throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException {
        final Method aMethod = getWrappedClass().getMethod( methodName, parameterTypes );
        return aMethod.invoke( getWrapped(), values );
    }

    protected Field getField( final String fieldName ) throws NoSuchFieldException {
        return getWrappedClass().getField( fieldName );
    }

    public Object getStaticFieldValue( final String fieldName ) {
        try {
            final Field staticField = getField( fieldName );

            if ( !Modifier.isStatic( staticField.getModifiers() ) ) {
                throw new IllegalArgumentException( "Field \"" + fieldName + "\" not static." );
            }

            return staticField.get( null );

        } catch ( final NoSuchFieldException nsfe ) {
            if ( logger.isEnabledFor( Level.ERROR ) )
                logger.error( "Unable to find static field \"" + fieldName + "\"", nsfe );
        } catch ( final IllegalAccessException iae ) {
            if ( logger.isEnabledFor( Level.ERROR ) )
                logger.error( "Unable to access static field \"" + fieldName + "\"", iae );
        }

        return null;
    }

    public Object getInstanceFieldValue( final String fieldName ) {
        try {
            final Field instanceField = getField( fieldName );

            if ( Modifier.isStatic( instanceField.getModifiers() ) ) {
                throw new IllegalArgumentException( "Field \"" + fieldName + "\" is static." );
            }

            return instanceField.get( null );

        } catch ( final NoSuchFieldException nsfe ) {
            if ( logger.isEnabledFor( Level.ERROR ) )
                logger.error( "Unable to find instance field \"" + fieldName + "\"", nsfe );
        } catch ( final IllegalAccessException iae ) {
            if ( logger.isEnabledFor( Level.ERROR ) )
                logger.error( "Unable to access instance field \"" + fieldName + "\"", iae );
        }

        return null;
    }
}
