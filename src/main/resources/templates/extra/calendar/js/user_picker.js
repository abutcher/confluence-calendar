function setCheckboxes()
{
    var numelements = document.selectorform.elements.length;
    var all = document.selectorform.elements.all;
    var users = document.selectorform.elements.users;

    if (users.length)
    {
        for (var i=1 ; i < users.length ; i++)
        {
            users[i].checked = all.checked;
        }
    }
    else if (users)
    {
        users.checked = all.checked;
    }
}

function selectUsers()
{
    var openerForm = opener.document.forms[formName];
    var openerEl = openerForm.elements[elementName];

    var item;
    var checkedList = "";

    var sep = "";
    var users = document.selectorform.elements.users;
    if (users)
    {
        if (users.length)
        {
            for (var i = 0 ; i < users.length; i++)
            {
                item = users[i];
                if (item.checked)
                {
                    checkedList  = checkedList + sep + item.value;
                    sep = ", ";
                }
            }
        }
        else if (users.checked)
        {
            checkedList = users.value;
        }
    }

    if (checkedList != "")
        openerEl.value = checkedList;

    window.close();
}

function toggleCheckBox(checkboxId, checkboxValue)
{
    var checkbox = document.selectorform.elements[checkboxId];

    if (checkbox)
    {
        if (checkbox.length)
        {
            for (var i = 0; i < checkbox.length; i++)
            {
                if (checkbox[i].value == checkboxValue)
                {
                    checkbox[i].checked = !checkbox[i].checked;
                }
            }
        }
        else
        {
            checkbox.checked = !checkbox.checked;
        }
    }
    return false;
}